---
title: "FAQ"
---

# Wie kann ich meine OER in OERSI einfügen?

Am besten wenden Sie sich mit der Anfrage an uns über das [Feedback-Formular](https://oersi.org/resources/services/contact). Schauen Sie zunächst, ob Ihre OER schon in der [Liste der zu ergänzenden Quellen](https://gitlab.com/oersi/oersi-etl/-/issues?label_name%5B%5D=add-source) aufgezählt sind und verlinken Sie in Ihrer Anfrage ggf. auf das entsprechende Ticket.

Prinzipiell sind wir sehr flexibel bei der Übernahme von Quellen in OERSI, d.h. es gibt verschiedene Möglichkeiten.

**1. Bereitstellung der Metadaten Ihrer OER**

Für die strukturierte Beschreibung von Lehr-/Lernressourcen mit schema.org/LRMI erarbeiten wir kooperativ (innerhalb der [OER-Metadatengruppe der DINI-AG KIM](https://wiki.dnb.de/display/DINIAGKIM/OER-Metadatengruppe)) das [Allgemeine Metadatenprofil für Bildungsmaterialien](https://w3id.org/kim/lrmi-profile/draft/). Dort wird auch angegeben, wie die strukturierten Daten bereitgestellt werden können. Eine Transformation Ihres bestehenden Formates ist in der Regel auch möglich.

**1.1. Im HTML eingebettetes schema.org + sitemap.xml**

Eine gute Lösung auch für SEO im Allgemeinen ist die Bereitstellung strukturierter Metadaten als eingebettes JSON-LD in den OER selbst verbunden mit einer [Sitemap](https://www.sitemaps.org), die uns die einzelnen Ressourcen finden lässt.

**1.2. JSON-API**

Ideal ist auch die Bereitstellung von JSON über eine HTTP-basierte API. Dies ist wesentlich performanter und resourcenschonender, wenn mehrere Datensätze auf einmal abgefragt werden können und nur die reinen Metadaten übertragen werden.

**1.3. Alternative Möglichkeiten**

Da wir wissen, dass wir nicht von allen Quellen erwarten können, den skizzierten Ansatz umzusetzen, unterstützt unserer Ansatz auch folgendes:

- XML über OAI-PMH
- HTML + Scraping
- Bereitstellung einer Bulk-Datei mit den stukturierten Daten (csv, JSON)

**2. Manuelles Verzeichnen in einer bereits indexierten Quelle**

Als weitere Möglichkeit möchte ich noch hinweisen auf die Erfassung der OER in einer Quelle, die bereits in OERSI indexiert wird. In Frage kommen da momentan vor allem [ZOERR](https://www.oerbw.de/), [Twillo](https://www.twillo.de) und [ORCA.nrw](https://orca.nrw/).

Der letztlich zu wählende Ansatz hängt von den genutzten Technologien und den damit verbundenen Möglichkeiten sowie den bereitstehenden Ressourcen für die Umsetzung ab. Haben Sie ausreichend technisch versierte Ressourcen bietet sich 1.) an. Ist technisches Personal rar, Sie haben aber Menschen, die die Ressourcen manuell erfassen können, dann wäre 2.) die beste Wahl.

# Kann ich OERSI über eine API einbinden?

Ja, Sie können die Daten aus OERSI sowohl on-thy-fly abfragen und direkt in Ihrer Anwendung verwenden als auch einen kompletten (Teil-) Abzug der Daten herunterladen und weiterverwenden.

Zur Zeit stellen wir die Elasticsearch-API von OERSI offen zur Verfügung. Da wir die Elasticsearch-API selbst für das OERSI-Frontend benutzen, können Sie die verwenden ohne zu fürchten, dass sie bald und plötzlich abgeschaltet wird. Zur Verwendung von Elasticsearch siehe
* https://www.elastic.co/guide/en/elasticsearch/reference/current/search-search.html
* https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl.html

## Beispiel on-thy-fly Suche

Suche nach "Klimawandel"

`curl -X 'POST' 'https://oersi.org/resources/api-internal/search/oer_data/_search?pretty' -H 'Content-Type: application/json' -H 'accept: application/json' -d '{"size":20,"from":0,"query": { "multi_match": { "query": "Klimawandel", "fields": ["name", "description", "keywords"]}},"sort": [{"id":"asc"}]}'
`

## Beispiel Datenabzug

Der stabilste Zugriff ist mit PIT (point in time). Siehe https://www.elastic.co/guide/en/elasticsearch/reference/current/point-in-time-api.html

### 1. PIT erstellen

`curl -X 'POST' 'https://oersi.org/resources/api-internal/search/oer_data/_pit?keep_alive=1m&pretty' -H 'accept: application/json'`
=> id für die weitere Verarbeitung merken (unten `<YOUR_PIT_ID>`)

### 2. Suchen (je 1000 Ergebnisse) hier am Beispiel twillo

#### 2.1 Suche für die ersten 1000 Treffer

`curl -X 'POST' 'https://oersi.org/resources/api-internal/search/_search?pretty' -H 'Content-Type: application/json' -H 'accept: application/json' -d '{"size":1000,"query": {"match": {"mainEntityOfPage.provider.name": "twillo"}},"pit": {"id": "<YOUR_PIT_ID>", "keep_alive": "1m"}, "sort": [{"id":"asc"}], "track_total_hits": true}'`

#### 2.2 YOUR_LAST_SORT_RESULT ermitteln ("sort" Eintrag des letzten HITs)

```
...
        "sort" : [
          "https://oer.identifier",
          35297
        ]
      }
    ]
  }
}
```

#### 2.3 Suche wiederholen bis keine Treffer mehr kommen

`curl -X 'POST' 'https://oersi.org/resources/api-internal/search/_search?pretty' -H 'Content-Type: application/json' -H 'accept: application/json' -d '{"size":1000,"query": {"match": {"mainEntityOfPage.provider.name": "twillo"}},"pit": {"id": "<YOUR_PIT_ID>", "keep_alive": "1m"}, "sort": [{"id":"asc"}], "track_total_hits": true, "search_after": <YOUR_LAST_SORT_RESULT>}'`

### 3. PIT löschen

`curl -X DELETE https://oersi.org/resources/api-internal/search/_pit -H 'Content-Type: application/json' -d '{"id":"<YOUR_PIT_ID>"}'`


# Warum erhalte ich doppelte Ergebnisse über die API?

Niemals on-thy-fly Such-Anfragen an [https://oersi.org/resources/api-internal/search/_search](https://oersi.org/resources/api-internal/search/_search) stellen - diese liefern Ergebnisse von mehreren Indices. Bei einer Suche über die API muss immer auch der Index `oer_data` angegeben werden: [https://oersi.org/resources/api-internal/search/oer_data/_search](https://oersi.org/resources/api-internal/search/oer_data/_search)

