---
title: About OERSI
subtitle: Search Index for Open Educational Resources
comments: false
---

OERSI blog provides a window into the development and approach for building the OER Search Index (OERSI). OERSI is a common project by [TIB Hannover](https://www.tib.eu/) and [North Rhine-Westphalian Library Service Centre (hbz)](https://www.hbz-nrw.de/) to create and maintain an open search index for educational resources. The current public protoype is running at [https://www.oersi.de/](https://www.oersi.de/).

The underlying software encompasses these modules:

- [oersi-backend](https://gitlab.com/oersi/oersi-backend) for the read/write-API & backend of the search index
- [oersi-etl](https://gitlab.com/oersi/oersi-etl) as framework to configure and execute ETL processes (extract, transform, load) for harvesting data from different sources and transforming it to a common structure for indexing
- [oersi-frontend](https://gitlab.com/oersi/oersi-frontend) for the search UI
- [oersi-setup](https://gitlab.com/oersi/oersi-setup) for knitting it all together and installing the index on a server

The issue backlog as well as closed and current issues can be viewed in the [project board](https://gitlab.com/groups/oersi/-/boards).